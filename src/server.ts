import Express, {Application, request, Request, Response} from 'express';
import cookieParser from 'cookie-parser';
import { User } from "./persistance/User";
import { Order } from "./persistance/Order";
import { sessionID } from "./persistance/SessionID";
import { UserSupport } from "./persistance/UserSupport";
import {Book} from "./persistance/Book";
import { createSessionMiddleware } from "./middleware/createSessionMiddleware";
import { retrieval} from "./middleware/retrievalSessionMiddleware";
import { StrongParamMiddleware } from "./middleware/StrongParamMiddleware";

const app: Application = Express();

import Session from "express-session";

const MongoStore = require('connect-mongo')(Session);

const Mongoose = require('mongoose');
const BodyParser = require('body-parser')

require('dotenv').config();

// SendGrid API set as environment variable
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


const {
    PORT = 8000,
} = process.env

app.use(BodyParser.json())

app.use(Express.json())

app.use(cookieParser("I am the Secret"))

Mongoose.connect('mongodb://localhost/BookStoreTest', { useNewUrlParser: true, useUnifiedTopology: true });
Mongoose.connection.once('open', () => console.log("Connected to database!"));

//======================================================================================================================
//route for creating users

app.post("/register", async(request:Request, response:Response) =>{

    const {firstName, lastName, emailID, password} = request.body;

    try{

        const existingUser = await User.findOne({ emailID: emailID });

        if (existingUser) {

            console.log(`A user with the email address '${emailID}' already exists, rejecting request with a 400`);

            return response.sendStatus(400).send("the user exists");
        }

        const addUser = await User.create({ firstName:firstName, lastName:lastName, emailID:emailID, password:password });

        console.log(addUser);

        return response.sendStatus(200);

    }catch (error) {

        console.log('An unexpected error occured: ' + error.message);

        return response.sendStatus(500);
    }


})

//======================================================================================================================
//route for creating session

app.post("/login", createSessionMiddleware, async (request: Request, response: Response) => {

    const {emailAddress, password} = request.body;

    const userExists: any = await User.findOne({ emailID: emailAddress}, {password: password} );
    console.log("User exists in inin"+userExists);

    // if(!userExists){
    //
    //         await User.create({emailID: 'anisha@gmail.com', firstName: 'Anisha', lastName: 'Parkar'});
    //     }

    console.log("cookie created" + response.locals.sessionCookieExists)

    console.log("login is here 1");

    if (response.locals.sessionCookieExists != "fail") {

        console.log("login is here 2");

        response.cookie("Cookie13", response.locals.sessionCookieExists, { signed: true })

        console.log("login is here 2 aa"+response.locals.sessionCookieExists);

        console.log(emailAddress);

        await sessionID.create({ emailID: emailAddress, password: password, sessionID: response.locals.sessionCookieExists });

        console.log("login is here 3");

        response.send(request.signedCookies)

    }
    else {
        response.status(400).send(request.signedCookies)
    }
})

//======================================================================================================================
//route for session authentication retrieval

app.get("/show", retrieval, (request, response) => {

    if (response.locals.userCookie == true) {

        response.status(200).send(" Cookies Exists")
    }
    else {

        response.status(400).send("Cookies does not exist")

    }
})

//======================================================================================================================
//route for session deletion

app.get("/delete", (request, response) => {

    response.clearCookie("Cookie13").send(" Cookie Deleted")

})

//======================================================================================================================
//route for book order using strong param middleware and sending email using SendGrid after order is placed.

app.post("/orderBook",
    [StrongParamMiddleware({ bookName: 'string', bookPrice:'string', orderQuantity: 'string' })],
    async (request: Request, response: Response) => {

        const strongparams: any = response.locals ?.strongParams;

        const bookName: string | undefined = strongparams ?.bookName;
        const bookPrice: string | undefined = strongparams ?.bookPrice;
        const orderQuantity: string | undefined = strongparams ?.orderQuantity;


        console.log(response.locals.strongParams.bookName + " this  " + bookName);
        console.log(response.locals.strongParams.bookPrice + " this  " + bookPrice);
        console.log(response.locals.strongParams.orderQuantity + " this  " + orderQuantity);

        const orderTotal = (response.locals.strongParams.bookPrice * response.locals.strongParams.orderQuantity)

        console.log(orderTotal);


        const order = await Order.create({
            bookName: bookName,
            bookPrice: bookPrice,
            orderQuantity: orderQuantity,
            orderTotal:orderTotal,

        });

        console.log(order)

        //If the user exists and order is placed, Email is sent to the user with orderID and orderTotal via SendGrid

        // const userExists: any = await sessionID.findOne({ sessionID: request.signedCookies.Cookie13}) ;
        //
        // const msg = {
        //     to: userExists.emailID,
        //     from: 'anishaparkar95@gmail.com',
        //     subject: 'Sending with Twilio SendGrid is Fun',
        //     text: 'Dear   ' +userExists.emailID+
        //            ',      Thanks for your order.       Your OrderID is:' +orderID+ '.   and the Order total is:' +orderTotal
        //     // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        // };
        //
        // console.log(msg);
        //
        // sgMail.send(msg);
        //
        // response.send("Order placed and email sent")
    }
)

//======================================================================================================================
// Generalized route for user support, user can send email to the admin about the wrong orders or about any problems.

app.post("/contactUs",
    [StrongParamMiddleware({ senderEmail: 'string', emailSubject:'string',emailText:'string'})],
    async (request: Request, response: Response) => {

        const strongparams: any = response.locals ?.strongParams;

        const senderEmail: string | undefined = strongparams ?.senderEmail;
        const emailSubject: string | undefined = strongparams ?.emailSubject;
        const emailText: string | undefined = strongparams ?.emailText;

        const userSupport = await UserSupport.create({
            senderEmail: senderEmail,
            emailSubject: emailSubject,
            emailText: emailText

        });

        console.log(userSupport);

        // const userExists: any = await sessionID.findOne({ sessionID: request.signedCookies.Cookie13}) ;
        // console.log('emailllllll user issss',userExists);

        const msg = {
            to: 'anishaparkar95@gmail.com',
            from: senderEmail,
            subject: emailSubject,
            text: emailText ,
            // html: '<strong>and easy to do anywhere, even with Node.js</strong>',
        };
        console.log(msg);

        sgMail.send(msg);

        response.status(400).send('email sent')


    }
)

//======================================================================================================================
//route for loading books from mongodb

app.get('/loadbooks', async(request,response)=>{

    console.log('A GET request came in asking for all Books');

    const allBooks = await Book.find({});

    return response.send(allBooks).status(200);

    })

//======================================================================================================================


app.listen(PORT, () => console.log(`https://localhost:${PORT}`));

