import { Document } from 'mongoose';

export interface ISessionID extends Document {

    emailID: string,
    password:string,
    sessionID: string,

}