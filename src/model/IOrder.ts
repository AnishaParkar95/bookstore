import {Document} from 'mongoose';

export interface IOrder extends Document {

    // orderID: string;
    // orderDate: string;

    bookName:string;
    bookPrice:string;
    // orderQuantity:number;
    orderQuantity:string;
    orderTotal: string;

}