import { Document } from 'mongoose';

export interface IUserSupport extends Document {

    senderEmail: string,
    emailSubject: string,
    emailText: string,

}