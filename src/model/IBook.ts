import {Document} from 'mongoose';

export interface IBook extends Document{

    bookID: string;
    bookName: string;

    category: string;
    description:string;

    publisher:string;
    releaseDate:string;

    editionDetails:string;

    bookPrice: number;
    quantity: number;

    image:string;



}