import { Document } from 'mongoose';

export interface IUser extends Document {

    emailID: string,
    password: string,

    firstName: string,
    lastName: string,

}