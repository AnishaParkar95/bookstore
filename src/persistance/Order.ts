
import Mongoose from 'mongoose';
import {IOrder} from "../model/IOrder";

const orderSchema: Mongoose.Schema<IOrder> =new Mongoose.Schema<IOrder>({

    // orderID:{
    //     type: String,
    //     required: true,
    //     validate:{
    //         validator: function(value:string): boolean{
    //
    //             /* orderID must be in alphanumerics,can be in range of 1-20, can contain - */
    //
    //             return value.length <= 20 && /^\w+[-]?(\w+[-]?)*$/.test(value);
    //         },
    //
    //         message:'OrderID can have alphanumerics and can contain "-"  '
    //     }
    // },

    // orderDate: {
    //     type: String,
    //     required: true
    // },

    bookName: {
        type: String,
        required: true
    },

    bookPrice: {
        type: String,
        required: true
    },

    orderQuantity: {
        type: String,
        required: true
    },

    orderTotal: {
        type: String,
        required: true
    },


    // orderQuantity: {
    //     type: Number,
    //     required: true,
    //     validate:{
    //
    //         validator: function (value:number): boolean {
    //
    //             /* order Quantity must be in numbers and can be in range of 1-5 */
    //
    //             return value <= 5 && /^[\d+]+$/.test(String(value));
    //         },
    //
    //         message:'Order Quantity must contain only numbers and should not be greater than 5'
    //     }
    // },

    // orderTotal: {
    //     type: Number,
    //     required: true,
    //     validate:{
    //
    //         validator: function (value:number): boolean {
    //
    //             /* Order total must be numbers and can contain . */
    //
    //             return value > 0 && /^[\d+.]+$/.test(String(value));
    //         },
    //
    //         message:'Order total must contain only numbers and can contain  . '
    //     }
    // },



})

export const Order = Mongoose.model<IOrder>('order', orderSchema );