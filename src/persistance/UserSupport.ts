import Mongoose from 'mongoose';
import {IUserSupport} from "../model/IUserSupport";

const userSupportSchema : Mongoose.Schema<IUserSupport> = new Mongoose.Schema<IUserSupport>({

    senderEmail: {
        type: String,
        required: true,
    },

    emailSubject: {
        type: String,
        required: true,
    },

    emailText: {
        type: String,
        required: true,
    }

})

export const UserSupport = Mongoose.model<IUserSupport>('UserSupport', userSupportSchema);