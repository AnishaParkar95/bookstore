import Mongoose from 'mongoose';
import {IBook} from "../model/IBook";


const bookSchema: Mongoose.Schema<IBook> =new Mongoose.Schema<IBook>({

    bookID: {
        type: String,
        required: true
    },

    bookName: {
        type: String,
        required: true,

        validate:{
            validator: function (value:string): boolean {

                /* Book name can in total be 200 letters from a-zA-Z0-9 and can contain space, dot and comma' */

                return value.length <= 100 && /^[\w+,.]+(\s{0,1}[\w+,. ])*$/.test(value);
            },

            message:'Book name can have alphanumerics and can contain  "."  ","  " " '
        }
    },

    category: {
        type: String,
        required: true
    },

    description: {
        type: String,
        required: true,
        validate: {

            validator: function (value: string): boolean {

                /* Description can in total be 500 letters from a-zA-Z0-9 and can contain space, dot and comma' */

                return value.length <= 500 && value.length > 20 && /^[\w+,.]+(\s{0,1}[\w+,. ])*$/.test(value);
            },

            message: 'Description can have alphanumerics and can contain  "."  ","  " " '
        }
    },

    publisher:{
        type:String,
        required:true,
    },

    releaseDate:{
        type:String,
        required:true,
    },

    bookPrice: {
        type: Number,
        required: true,
        validate:{

            validator: function (value:number): boolean {

                /* BookPrice must be numbers and can contain . */

                return value > 0 && /^[\d+.]+$/.test(String(value));
            },

            message:'Book Price must contain only numbers and can contain  . '
        }
    },

    quantity: {
        type: Number,
        required: true,
        validate:{

            validator: function (value:number): boolean {

                /* Quantity must be in numbers */

                return value > 0 && /^[\d]+$/.test(String(value));
            },

            message:'Quantity must contain only numbers '
        }
    },

    image: {
        type: String,
        required: true
    },

});

bookSchema.virtual('editionDetails').get(function (this:IBook) {

    return this.publisher+ '' + this.releaseDate;

}).set(function (this:IBook, editionDetails:string) {

    if(!editionDetails.includes(',')){
        throw new Error('Edition Details must have comma between publisher and edition details');
    }

    const [publisher, releaseDate]: string[] = editionDetails.split(',');

    this.publisher = publisher;
    this.releaseDate = releaseDate;

});

export const Book = Mongoose.model<IBook>('book', bookSchema );