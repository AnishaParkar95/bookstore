
import Mongoose from 'mongoose';
import {ISessionID} from "../model/ISessionID";

const sessionIDSchema : Mongoose.Schema<ISessionID> = new Mongoose.Schema<ISessionID>({

    emailID: {
        type: String,
        required: true,
    },

    password: {
        type: String,
        required: true,
    },

    sessionID: {
        type: String,
        required: true,
        }


})

export const sessionID = Mongoose.model<ISessionID>('sessionID', sessionIDSchema);