import Mongoose from "mongoose";

import { User } from "./persistance/User";
import {Order} from "./persistance/Order";
import {Book} from "./persistance/Book";


// Playground for interfacing with Mongoose
(async () => {

    await Mongoose.connect('mongodb://localhost:27017/BookStore', { useNewUrlParser: true });

    const user = await new User({
        firstName: 'Peter Andrew', lastName: 'Parker',
        emailID:'Peter-parker9@gmail.com.edu'
    });

    const book = await new Book({
        bookID:'Web22020',
        bookName:'Bush Eric. 2018, Blue Sky Productions',
        category:'academics',
        description:'Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
        bookPrice: '20.94',
        quantity: '20',
        editionDetails:'Blue Sky Productions,2018',
    });


    const order = await new Order({
        orderID:'02172020-01-P',orderDate:'02/17/2020',
        orderQuantity:'4', orderTotal: '40'


    });


    const savedUser = await user.save();
    const savedBook = await book.save();
    const savedOrder = await order.save();

    console.log(savedUser, savedBook ,savedOrder);

    process.exit(0);

})();
