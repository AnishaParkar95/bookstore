import {NextFunction, Request, RequestHandler, Response} from "express";


export function StrongParamMiddleware(params:any): RequestHandler {

    return (request:Request, response:Response, next:NextFunction) => {

        const strongParams:any = {};
        const weakParams = request.body;

        Object.entries(params).forEach(([key, value])=>  {

            const weakParam = weakParams[key];

            if(weakParams && weakParam !== undefined && typeof weakParam === value){

                console.log(key + " this is the req . body [key] " + weakParam)
                console.log("this is the value " + value)
                strongParams[key] = weakParam;

            }
        });

        response.locals.strongParams = strongParams;

        request.body = null;

        return next();

    }

}