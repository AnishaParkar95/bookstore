"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function StrongParamMiddleware(params) {
    return function (request, response, next) {
        var strongParams = {};
        var weakParams = request.body;
        Object.entries(params).forEach(function (_a) {
            var key = _a[0], value = _a[1];
            var weakParam = weakParams[key];
            if (weakParams && weakParam !== undefined && typeof weakParam === value) {
                console.log(key + " this is the req . body [key] " + weakParam);
                console.log("this is the value " + value);
                strongParams[key] = weakParam;
            }
        });
        response.locals.strongParams = strongParams;
        request.body = null;
        return next();
    };
}
exports.StrongParamMiddleware = StrongParamMiddleware;
