"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var cookie_parser_1 = __importDefault(require("cookie-parser"));
var User_1 = require("./persistance/User");
var Order_1 = require("./persistance/Order");
var SessionID_1 = require("./persistance/SessionID");
var UserSupport_1 = require("./persistance/UserSupport");
var Book_1 = require("./persistance/Book");
var createSessionMiddleware_1 = require("./middleware/createSessionMiddleware");
var retrievalSessionMiddleware_1 = require("./middleware/retrievalSessionMiddleware");
var StrongParamMiddleware_1 = require("./middleware/StrongParamMiddleware");
var app = express_1.default();
var express_session_1 = __importDefault(require("express-session"));
var MongoStore = require('connect-mongo')(express_session_1.default);
var Mongoose = require('mongoose');
var BodyParser = require('body-parser');
require('dotenv').config();
// SendGrid API set as environment variable
var sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
var _a = process.env.PORT, PORT = _a === void 0 ? 8000 : _a;
app.use(BodyParser.json());
app.use(express_1.default.json());
app.use(cookie_parser_1.default("I am the Secret"));
Mongoose.connect('mongodb://localhost/BookStoreTest', { useNewUrlParser: true, useUnifiedTopology: true });
Mongoose.connection.once('open', function () { return console.log("Connected to database!"); });
//======================================================================================================================
//route for creating users
app.post("/register", function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, firstName, lastName, emailID, password, existingUser, addUser, error_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = request.body, firstName = _a.firstName, lastName = _a.lastName, emailID = _a.emailID, password = _a.password;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 4, , 5]);
                return [4 /*yield*/, User_1.User.findOne({ emailID: emailID })];
            case 2:
                existingUser = _b.sent();
                if (existingUser) {
                    console.log("A user with the email address '" + emailID + "' already exists, rejecting request with a 400");
                    return [2 /*return*/, response.sendStatus(400).send("the user exists")];
                }
                return [4 /*yield*/, User_1.User.create({ firstName: firstName, lastName: lastName, emailID: emailID, password: password })];
            case 3:
                addUser = _b.sent();
                console.log(addUser);
                return [2 /*return*/, response.sendStatus(200)];
            case 4:
                error_1 = _b.sent();
                console.log('An unexpected error occured: ' + error_1.message);
                return [2 /*return*/, response.sendStatus(500)];
            case 5: return [2 /*return*/];
        }
    });
}); });
//======================================================================================================================
//route for creating session
app.post("/login", createSessionMiddleware_1.createSessionMiddleware, function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, emailAddress, password, userExists;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = request.body, emailAddress = _a.emailAddress, password = _a.password;
                return [4 /*yield*/, User_1.User.findOne({ emailID: emailAddress }, { password: password })];
            case 1:
                userExists = _b.sent();
                console.log("User exists in inin" + userExists);
                // if(!userExists){
                //
                //         await User.create({emailID: 'anisha@gmail.com', firstName: 'Anisha', lastName: 'Parkar'});
                //     }
                console.log("cookie created" + response.locals.sessionCookieExists);
                console.log("login is here 1");
                if (!(response.locals.sessionCookieExists != "fail")) return [3 /*break*/, 3];
                console.log("login is here 2");
                response.cookie("Cookie13", response.locals.sessionCookieExists, { signed: true });
                console.log("login is here 2 aa" + response.locals.sessionCookieExists);
                console.log(emailAddress);
                return [4 /*yield*/, SessionID_1.sessionID.create({ emailID: emailAddress, password: password, sessionID: response.locals.sessionCookieExists })];
            case 2:
                _b.sent();
                console.log("login is here 3");
                response.send(request.signedCookies);
                return [3 /*break*/, 4];
            case 3:
                response.status(400).send(request.signedCookies);
                _b.label = 4;
            case 4: return [2 /*return*/];
        }
    });
}); });
//======================================================================================================================
//route for session authentication retrieval
app.get("/show", retrievalSessionMiddleware_1.retrieval, function (request, response) {
    if (response.locals.userCookie == true) {
        response.status(200).send(" Cookies Exists");
    }
    else {
        response.status(400).send("Cookies does not exist");
    }
});
//======================================================================================================================
//route for session deletion
app.get("/delete", function (request, response) {
    response.clearCookie("Cookie13").send(" Cookie Deleted");
});
//======================================================================================================================
//route for book order using strong param middleware and sending email using SendGrid after order is placed.
app.post("/orderBook", [StrongParamMiddleware_1.StrongParamMiddleware({ bookName: 'string', bookPrice: 'string', orderQuantity: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongparams, bookName, bookPrice, orderQuantity, orderTotal, order;
    var _a, _b, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                strongparams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                bookName = (_b = strongparams) === null || _b === void 0 ? void 0 : _b.bookName;
                bookPrice = (_c = strongparams) === null || _c === void 0 ? void 0 : _c.bookPrice;
                orderQuantity = (_d = strongparams) === null || _d === void 0 ? void 0 : _d.orderQuantity;
                console.log(response.locals.strongParams.bookName + " this  " + bookName);
                console.log(response.locals.strongParams.bookPrice + " this  " + bookPrice);
                console.log(response.locals.strongParams.orderQuantity + " this  " + orderQuantity);
                orderTotal = (response.locals.strongParams.bookPrice * response.locals.strongParams.orderQuantity);
                console.log(orderTotal);
                return [4 /*yield*/, Order_1.Order.create({
                        bookName: bookName,
                        bookPrice: bookPrice,
                        orderQuantity: orderQuantity,
                        orderTotal: orderTotal,
                    })];
            case 1:
                order = _e.sent();
                console.log(order);
                return [2 /*return*/];
        }
    });
}); });
//======================================================================================================================
// Generalized route for user support, user can send email to the admin about the wrong orders or about any problems.
app.post("/contactUs", [StrongParamMiddleware_1.StrongParamMiddleware({ senderEmail: 'string', emailSubject: 'string', emailText: 'string' })], function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var strongparams, senderEmail, emailSubject, emailText, userSupport, msg;
    var _a, _b, _c, _d;
    return __generator(this, function (_e) {
        switch (_e.label) {
            case 0:
                strongparams = (_a = response.locals) === null || _a === void 0 ? void 0 : _a.strongParams;
                senderEmail = (_b = strongparams) === null || _b === void 0 ? void 0 : _b.senderEmail;
                emailSubject = (_c = strongparams) === null || _c === void 0 ? void 0 : _c.emailSubject;
                emailText = (_d = strongparams) === null || _d === void 0 ? void 0 : _d.emailText;
                return [4 /*yield*/, UserSupport_1.UserSupport.create({
                        senderEmail: senderEmail,
                        emailSubject: emailSubject,
                        emailText: emailText
                    })];
            case 1:
                userSupport = _e.sent();
                console.log(userSupport);
                msg = {
                    to: 'anishaparkar95@gmail.com',
                    from: senderEmail,
                    subject: emailSubject,
                    text: emailText,
                };
                console.log(msg);
                sgMail.send(msg);
                response.status(400).send('email sent');
                return [2 /*return*/];
        }
    });
}); });
//======================================================================================================================
//route for loading books from mongodb
app.get('/loadbooks', function (request, response) { return __awaiter(void 0, void 0, void 0, function () {
    var allBooks;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                console.log('A GET request came in asking for all Books');
                return [4 /*yield*/, Book_1.Book.find({})];
            case 1:
                allBooks = _a.sent();
                return [2 /*return*/, response.send(allBooks).status(200)];
        }
    });
}); });
//======================================================================================================================
app.listen(PORT, function () { return console.log("https://localhost:" + PORT); });
