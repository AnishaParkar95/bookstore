"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var userSchema = new mongoose_1.default.Schema({
    emailID: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                /*  the input string cannot begin with . or -
                cannot contain '..', '--', '.-' or '-.'
                can contain '.' or '-' after first character followed by [a-zA-Z0-9_]
                can contain '.com', '.edu.sg'  */
                return value.length > 0 && /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(value);
            },
            message: ' Email must contain @ and - . are optional (Peter.parker9@gmail.com.edu) '
        }
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                /*First name can in total be 30 letters from a-zA-Z and can contain space, eg. if the First name is Peter Andrew*/
                return value.length <= 30 && /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/.test(value);
            },
            message: 'First name must have letters and cannot be empty'
        }
    },
    lastName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                /*Last name can in total 2-30 letters from a-zA-Z*/
                return value.length > 2 && value.length <= 30 && /^[a-zA-Z]*$/.test(value);
            },
            message: 'Last name must have range of 3-30 letters'
        }
    },
});
exports.User = mongoose_1.default.model('user', userSchema);
