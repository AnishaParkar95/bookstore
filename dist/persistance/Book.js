"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var bookSchema = new mongoose_1.default.Schema({
    bookID: {
        type: String,
        required: true
    },
    bookName: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                /* Book name can in total be 200 letters from a-zA-Z0-9 and can contain space, dot and comma' */
                return value.length <= 100 && /^[\w+,.]+(\s{0,1}[\w+,. ])*$/.test(value);
            },
            message: 'Book name can have alphanumerics and can contain  "."  ","  " " '
        }
    },
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
        validate: {
            validator: function (value) {
                /* Description can in total be 500 letters from a-zA-Z0-9 and can contain space, dot and comma' */
                return value.length <= 500 && value.length > 20 && /^[\w+,.]+(\s{0,1}[\w+,. ])*$/.test(value);
            },
            message: 'Description can have alphanumerics and can contain  "."  ","  " " '
        }
    },
    publisher: {
        type: String,
        required: true,
    },
    releaseDate: {
        type: String,
        required: true,
    },
    bookPrice: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                /* BookPrice must be numbers and can contain . */
                return value > 0 && /^[\d+.]+$/.test(String(value));
            },
            message: 'Book Price must contain only numbers and can contain  . '
        }
    },
    quantity: {
        type: Number,
        required: true,
        validate: {
            validator: function (value) {
                /* Quantity must be in numbers */
                return value > 0 && /^[\d]+$/.test(String(value));
            },
            message: 'Quantity must contain only numbers '
        }
    },
    image: {
        type: String,
        required: true
    },
});
bookSchema.virtual('editionDetails').get(function () {
    return this.publisher + '' + this.releaseDate;
}).set(function (editionDetails) {
    if (!editionDetails.includes(',')) {
        throw new Error('Edition Details must have comma between publisher and edition details');
    }
    var _a = editionDetails.split(','), publisher = _a[0], releaseDate = _a[1];
    this.publisher = publisher;
    this.releaseDate = releaseDate;
});
exports.Book = mongoose_1.default.model('book', bookSchema);
