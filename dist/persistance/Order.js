"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var orderSchema = new mongoose_1.default.Schema({
    // orderID:{
    //     type: String,
    //     required: true,
    //     validate:{
    //         validator: function(value:string): boolean{
    //
    //             /* orderID must be in alphanumerics,can be in range of 1-20, can contain - */
    //
    //             return value.length <= 20 && /^\w+[-]?(\w+[-]?)*$/.test(value);
    //         },
    //
    //         message:'OrderID can have alphanumerics and can contain "-"  '
    //     }
    // },
    // orderDate: {
    //     type: String,
    //     required: true
    // },
    bookName: {
        type: String,
        required: true
    },
    bookPrice: {
        type: String,
        required: true
    },
    orderQuantity: {
        type: String,
        required: true
    },
    orderTotal: {
        type: String,
        required: true
    },
});
exports.Order = mongoose_1.default.model('order', orderSchema);
