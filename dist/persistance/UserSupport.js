"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var userSupportSchema = new mongoose_1.default.Schema({
    senderEmail: {
        type: String,
        required: true,
    },
    emailSubject: {
        type: String,
        required: true,
    },
    emailText: {
        type: String,
        required: true,
    }
});
exports.UserSupport = mongoose_1.default.model('UserSupport', userSupportSchema);
