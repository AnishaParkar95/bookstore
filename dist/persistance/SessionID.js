"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var sessionIDSchema = new mongoose_1.default.Schema({
    emailID: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    sessionID: {
        type: String,
        required: true,
    }
});
exports.sessionID = mongoose_1.default.model('sessionID', sessionIDSchema);
