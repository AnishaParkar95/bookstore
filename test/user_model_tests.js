
const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const { User } = require('../dist/persistance/User');

describe('User model tests', function () {

    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/BookStore', { useNewUrlParser: true });
    });

    beforeEach(async function () {

        await User.deleteMany({});
    });

//==============================================================================================================================================

    describe('Email validation tests', function () {

        it('Should prevent setting an input email beginning from dot(.) ', async function () {

            const user = await new User({ emailID: '.Peter-parker9@gmail.com.edu', lastName: 'Parker' , firstName: 'Peter Andrew'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: emailID:  Email must contain @ and - . are optional (Peter.parker9@gmail.com.edu) ')
            }
        });

        it('Should prevent setting an input email without @', async function () {

            const user = await new User({ emailID: 'Peter-parker9gmail.com.edu', lastName: 'Parker' , firstName: 'Peter Andrew'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: emailID:  Email must contain @ and - . are optional (Peter.parker9@gmail.com.edu) ')
            }
        });

        it('Should prevent setting an input email containing ..  -- .-  -. ', async function () {

            const user = await new User({ emailID: 'Peter-.parker9@gmail..com.edu', lastName: 'Parker' , firstName: 'Peter Andrew'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: emailID:  Email must contain @ and - . are optional (Peter.parker9@gmail.com.edu) ')
            }
        });

    });

//=============================================================================================================================================

    describe('First name validation tests', function () {

        it('Should prevent setting first name length >30', async function () {

            const user = await new User({ firstName: 'anishavikasparkaranishavikasparkar', lastName: 'Parker',  emailID:'Peter-parker9@gmail.com.edu'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: firstName: First name must have letters and cannot be empty')
                // expect(user.firstName).to.have.lengthOf( 1);
            }
        });

        it('Should prevent setting a non-alpha first name', async function () {

            const user = await new User({ firstName: '12345', lastName: 'Parker',  emailID:'Peter-parker9@gmail.com.edu'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: firstName: First name must have letters and cannot be empty')
                 // expect(user.firstName).to.be.a('string');
            }
        });

    });
//=============================================================================================================================================

    describe('Last name validation tests', function () {

        it('Should prevent setting last name length < 3', async function () {

            const user = await new User({ firstName: 'Peter', lastName: 'Pa',  emailID:'Peter-parker9@gmail.com.edu'});

            try {
                await user.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('user validation failed: lastName: Last name must have range of 3-30 letters')

            }
        });

    });


});
