const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const { Order } = require('../dist/persistance/Order');

describe('Order model tests', function () {


    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/BookStore', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await Order.deleteMany({});
    });

//==============================================================================================================================================

    describe('Order validation tests', function () {

        it('Should prevent setting orderID containing --', async function () {

            const order = await new Order({ orderID:'02172020--01P',
                                            orderDate:'02/17/2020',
                                            orderQuantity:'2', orderTotal: '40'});

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderID: OrderID can have alphanumerics and can contain "-"  ')

            }
        });

        it('Should prevent setting orderID starting with special characters', async function () {

            const order = await new Order({ orderID:'@@02172020-01P',
                orderDate:'02/17/2020',
                orderQuantity:'2', orderTotal: '40'});

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderID: OrderID can have alphanumerics and can contain "-"  ')

            }
        });

        it('Should prevent setting orderID with length > 20', async function () {

            const order = await new Order({ orderID:'02172020-01P-2020-January7-2020',
                orderDate:'02/17/2020',
                orderQuantity:'2', orderTotal: '40'});

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderID: OrderID can have alphanumerics and can contain "-"  ')

            }
        });

        it('Should prevent setting orderID with special character except - ', async function () {

            const order = await new Order({ orderID:'02172020.01P',
                orderDate:'02/17/2020',
                orderQuantity:'2', orderTotal: '40'});

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderID: OrderID can have alphanumerics and can contain "-"  ')

            }
        });

    });

//==============================================================================================================================================

    describe('Order validation tests', function () {

        it('Should prevent setting orderQuantity > 5 ', async function () {

            const order = await new Order({  orderQuantity: '7',
                orderTotal: '40',
                orderID: '02172020-01P',
                orderDate: '02/17/2020',
            });

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderQuantity: Order Quantity must contain only numbers and should not be greater than 5')

            }
        });
    });

//==============================================================================================================================================
    describe('Order validation tests', function () {

        it('Should prevent setting orderTotal to 0', async function () {

            const order = await new Order({ orderTotal: '0',
                orderQuantity: '5',
                orderID: '02172020-01P',
                orderDate: '02/17/2020',
            });

            try {
                await order.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('order validation failed: orderTotal: Order total must contain only numbers and can contain  . ')

            }
        });
    });


});