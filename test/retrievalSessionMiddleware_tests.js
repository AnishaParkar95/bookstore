// const {retrieval} =  require("../dist/middleware/retrievalSessionMiddleware");
// const Mongoose = require('mongoose');
// const httpMocks = require('node-mocks-http');
// const sinon = require('sinon');
// const expect = require('chai').expect;
// const nanoid = require('nanoid')
//
// describe('Session model tests', function () {
//
//
//     before(async function () {
//
//         await Mongoose.connect('mongodb://localhost:27017/BookStoreTest', {useNewUrlParser: true});
//     });
//
//
// describe('Authentication middleware tests', () => {
//
//     it('allows a valid user', async () => {
//
//         // console.log("hello")
//
//         const request = httpMocks.createRequest({ signedCookies: { Cookie13: "awWoJ6zd8Gz4Z64T2CeTCa9YcjtCeJ" },headers: { emailid: "anishaparkar95@gmail.com"} });
//         const response = httpMocks.createResponse();
//         const next = sinon.spy();
//
//         // console.log("hello me request " ,request)
//
//
//         await retrieval(request, response, next);
//
//         expect(response.locals.userCookie).to.be.true;
//
//     });
//
//     it('blocks an invalid user', async() => {
//
//         // This dummy function gives a bad session ID, yours would either populate with nothing or
//         // point to a session ID that no longer exists in the database
//
//
//         const request = httpMocks.createRequest({ signedCookies: { Cookie13: "awWoJ6zd8Gz4Z64T2CeTCa9Ycjt" },headers: { emailid: "anishaparkar95@gmail.com"} });
//         const response = httpMocks.createResponse();
//         const next = sinon.spy();
//
//         // console.log("hello me request " ,request)
//
//
//         await retrieval(request, response, next);
//
//         expect(response.locals.userCookie).to.be.false;
// });
//
//
// });
// });