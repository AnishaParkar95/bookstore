const chai = require('chai');
const expect = chai.expect;

const Mongoose = require('mongoose');

const { Book } = require('../dist/persistance/Book');

describe('Book model tests', function () {


    before(async function () {

        await Mongoose.connect('mongodb://localhost:27017/BookStore', {useNewUrlParser: true});
    });

    beforeEach(async function () {

        await Book.deleteMany({});
    });

//==============================================================================================================================================
    describe('Edition Details virtual attribute tests', function () {

        it('Should allow setting a editionDetails virtual', async function () {

            const book = await new Book({publisher: 'PUBLISHER', releaseDate: 'RELEASE_DATE',
                                        bookID:'Web22020',
                                        bookName:'Bush Eric. 2018, Blue Sky Productions',
                                        category:'academics',
                                        description:'Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
                                        bookPrice: '20.9',
                                        quantity: '15'
                                        });

            expect(book.publisher).to.equal('PUBLISHER');
            expect(book.releaseDate).to.equal('RELEASE_DATE');

            book.editionDetails = 'Blue Sky Productions,2018';

            expect(book.publisher).to.equal('Blue Sky Productions');
            expect(book.releaseDate).to.equal('2018');

            const savedBook = await book.save();

            expect(savedBook.publisher).to.equal('Blue Sky Productions');
            expect(savedBook.releaseDate).to.equal('2018');
        });
    });

//==============================================================================================================================================

    describe('Book name validation tests', function () {

        it('Should prevent setting book name with special characters except . and ,', async function () {

            const book = await new Book({ bookName:'Bush $Eric 2018 %Blue Sky Productions',
                publisher: 'PUBLISHER', releaseDate: 'RELEASE_DATE',
                bookID:'Web22020',
                category:'academics',
                description:'Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
                bookPrice: '20.9',
                quantity: '15'});

            try {
                await book.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('book validation failed: bookName: Book name can have alphanumerics and can contain  "."  ","  " " ')
            }
        });

        it('Should prevent setting book name starting with special characters', async function () {

            const book = await new Book({ bookName:'...Bush $Eric 2018 %Blue Sky Productions',
                publisher: 'PUBLISHER', releaseDate: 'RELEASE_DATE',
                bookID:'Web22020',
                category:'academics',
                description:'Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
                bookPrice: '20.9',
                quantity: '15'});

            try {
                await book.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('book validation failed: bookName: Book name can have alphanumerics and can contain  "."  ","  " " ')
            }
        });

        it('Should prevent setting book name with more than 1 space', async function () {

            const book = await new Book({ bookName:'...  Bush $Eric 2018,   %Blue Sky Productions',
                publisher: 'PUBLISHER', releaseDate: 'RELEASE_DATE',
                bookID:'Web22020',
                category:'academics',
                description:'Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
                bookPrice: '20.9',
                quantity: '15'});

            try {
                await book.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('book validation failed: bookName: Book name can have alphanumerics and can contain  "."  ","  " " ')
            }
        });
    });

//==============================================================================================================================================

    describe('Book Description validation tests', function () {

        it('Should prevent setting book description starting with special characters', async function () {

            const book = await new Book({description:'######   Node.js, MongoDb, React, React Native FullStack Fundamentals and Beyond',
                bookName:'Bush Eric. 2018, Blue Sky Productions',
                publisher: 'PUBLISHER', releaseDate: 'RELEASE_DATE',
                bookID:'Web22020',
                category:'academics',
                bookPrice: '20.9',
                quantity: '15'});

            try {
                await book.save();
                expect.fail("Expected error not thrown");

            } catch (error) {
                expect(error.message).to.equal('book validation failed: description: Description can have alphanumerics and can contain  "."  ","  " " ')
            }
        });
    });


});